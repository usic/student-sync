<?php
/*
// This script provides sync students data
// between ICC and USIC.
// @dzubchik, dmytro@dzubenko.name, 2013 
*/

$config = array(
    'icc' =>
    array(
        'host' => '',
        'user' => '',
        'pass' => '',
        'db'   => ''
    ),
    'usic'=>
    array(
        'host'=>'',
        'user'=>'',
        'pass'=>'',
        'db'=>''
    )
);

function convert(&$item)
{
    $item = iconv("cp1251", "UTF-8", $item);
    $from = array('A','B','C','E','H','I','K','O','P','T','X','Y');
    $to = array('А','В','С','Е','Н','І','К','О','Р','Т','Х','У');
    $item = str_replace($from, $to, $item);
}

$iccConnection = mssql_connect($config['icc']['host'], $config['icc']['user'], $config['icc']['pass']);
if (!$iccConnection) {
    die('Not connected : ' . mssql_get_last_message());
}

$db_selected = mssql_select_db($config['icc']['db'], $iccConnection);
if (!$db_selected) {
    die ('Can\'t use db : ' . mssql_get_last_message());
}
//get data
$query = "Select * from View_For_Student_DB";
$result = mssql_query($query);
if (!$result) {
    die('Invalid query: ' . mssql_get_last_message());
}

//now we can connect to usic
$conn = new Mongo("mongodb://" . $config['usic']['host'],
    array("db" => $config['usic']['db'], "username" => $config['usic']['user'], "password" => $config['usic']['pass'])
);
if (!$conn) {
    die('Can not connect to mongo');
}
$c = $conn->$config['usic']['db']->users;
while ($row = mssql_fetch_assoc($result)) {
    $data['surName'] = $row['LName'];
    $data['firstName'] = $row['FName'];
    $data['middleName'] = $row['MName'];
    $data['faculty'] = $row["Facultet"];
    $data['profession'] = $row['Special'];
    $data['degree'] = $row['Kval'];
    $data['pasCode'] = $row['PasChar'] . $row['PasNum'];
    array_walk_recursive($data, 'convert');
    $data['year'] = (int)$row['Year'];
    unset($row);
    $keys = array("surName" => $data["surName"], "pasCode" => $data["pasCode"]);
    $c->findAndModify($keys, array('$set' => $data), null, array("upsert" => true));
}
$c->ensureIndex(array('surName' => 1,'pasCode'=>1), array("unique" => true));
